import React from 'react';
import './App.css';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter as Router } from 'react-router-dom';
import Arrendatario from './components/arrendatario/Arrendatario';
import { Tabs, Tab, Typography, Box, makeStyles, Toolbar } from '@material-ui/core';
import Operador from './components/operador/Operador';
import Propiedad from './components/propiedad/Propiedad';
import NavBar from './components/appBar/NavBar';
import { StateProvider } from './services/contextService';

const client = new ApolloClient({
  uri: 'https://48p1r2roz4.sse.codesandbox.io'
})

const TabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

const a11yProps = (index) => {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 'calc(100vh - 64px)'
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  tabpanel: {
    width: '100%'
  }
}));

const initialState = {
  transactions: [
      {id: 0, destinyId: 1, status: 'Pendiente', property: 'Propiedad 1', account: 'Arrendatario', destiny: 'Propiedad', type: 'Abono', category: 'Arriendo Mensual',
        ammount: 2200, currency: '$', imputable: false, contableDate: new Date(), transactionDate: new Date(), overDate: new Date()},
      {id: 1, destinyId: 0, status: 'Pendiente', property: 'Propiedad 1', account: 'Propiedad', destiny: 'Arrendatario', type: 'Cargo', category: 'Arriendo Mensual',
        ammount: 2200, currency: '$', imputable: false, contableDate: new Date(), transactionDate: new Date(), overDate: new Date()},
      {id: 2, destinyId: 3, status: 'Pendiente', property: 'Propiedad 3', account: 'Arrendatario', destiny: 'Operador', type: 'Cargo', category: 'Cargo por Cuenta',
        ammount: 600, currency: '$', imputable: true, contableDate: new Date(), transactionDate: new Date(), overDate: new Date()},
      {id: 3, destinyId: 2, status: 'Pendiente', property: 'Propiedad 3', account: 'Operador', destiny: 'Arrendatario', type: 'Abono', category: 'Cargo por Cuenta',
        ammount: 600, currency: '$', imputable: true, contableDate: new Date(), transactionDate: new Date(), overDate: new Date()},
  ],
  currentID: 4,
}

const reducer = (state, action) => {
  switch (action.type) {
      case 'changeTransaction':
        console.log('Action', action);
        return {
          ...state,
          transactions: action.newTransactions
        };
      case 'changeID':
        return {
          ...state,
          currentID: state.currentID+2
        }
      default:
          return state;
  }
}

function App() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  return (
    <StateProvider initialState={initialState} reducer={reducer}>
      <div className={classes.root}>
        <ApolloProvider client={client}>
          <Router>
            <NavBar />
            <Toolbar />
            <div className={classes.content}>
              <Tabs
                orientation="vertical"
                variant="scrollable"
                aria-label="Vertical tabs example"
                value={value}
                onChange={handleChange}
                className={classes.tabs}
              >
                <Tab label="Operador" {...a11yProps(0)} />
                <Tab label="Arrendatarios" {...a11yProps(1)} />
                <Tab label="Propiedades" {...a11yProps(2)} />
              </Tabs>
              <TabPanel value={value} index={0} className={classes.tabpanel}>
                <Operador />
              </TabPanel>
              <TabPanel value={value} index={1} className={classes.tabpanel}>
                <Arrendatario />
              </TabPanel>
              <TabPanel value={value} index={2} className={classes.tabpanel}>
                <Propiedad />
              </TabPanel>
            </div>
          </Router>
        </ApolloProvider>
      </div>
    </StateProvider>
  );
}

export default App;
