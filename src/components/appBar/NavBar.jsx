import React from 'react'
import { AppBar, Toolbar, Typography } from '@material-ui/core';

export default function NavBar() {
    return (
        <AppBar>
            <Toolbar>
                <Typography variant="h6">Modulo Contable</Typography>
            </Toolbar>
        </AppBar>
    )
}
