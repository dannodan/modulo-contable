import React, { useState, useEffect } from 'react'
import { getState } from '../../services/contextService';
import { Container, Grid, Paper, Fab, makeStyles, Typography, Button, Divider, TextField, MenuItem } from '@material-ui/core';
import MaterialTable from 'material-table';
import { MaterialIcons } from '../../constants/materialIcons';
import { Edit, Delete, Add } from '@material-ui/icons';
import TransactionModal from '../modals/TransactionModal';
import ConsolidatedModal from '../modals/ConsolidatedModal';
import { format } from 'date-fns';
import { Properties } from '../../constants/constants';
	
const useStyles = makeStyles(theme => ({
	status: {
		padding: theme.spacing(1, 1),
		backgroundColor: '#ea0',
		color: '#fff',
		width: 'max-content',
		fontWeight: 'bold',
		textTransform: 'uppercase'
	},
	fab: {
		position: 'fixed',
		bottom: 20,
		right: 20
	},
	pullRight: {
		float: 'right'
	},
	divider: {
		marginBottom: '40px'
	}
}))

export default function Arrendatario() {

	const [transaction, setTransaction] = useState({});
	const [openTransaction, setOpenTransaction] = useState(false);
	const [openConsolidated, setOpenConsolidated] = useState(false);
	const [edit, setEdit] = useState(false);
	const [{transactions, currentID}, dispatch] = getState();
	const [property, setProperty] = useState('Todas');
	const [tableData, setTableData] = useState([]);

	useEffect(() => {
		let transactionData = transactions.filter(entry => entry.account === 'Arrendatario');
		if (property !== 'Todas') {
			transactionData = transactionData.filter(entry => property === entry.property);
		}
		setTableData(transactionData);
	}, [transactions, property])

	const classes = useStyles();
	
	const handleCloseTransaction = () => {
		setOpenTransaction(false);
	}

	const handleCloseConsolidated = () => {
		setOpenConsolidated(false);
	}

	const viewConsolidated = () => {
		setOpenConsolidated(true);
	}

	const createTransaction = () => {
		console.log("Creando nueva transaccion");
		setOpenTransaction(true);
		setTransaction({});
	}

	const editTransaction = (event, rowData) => {
		setEdit(true);
		setOpenTransaction(true);
		setTransaction(rowData);
	}

	const deleteTransaction = (event, rowData) => {
		const newTransactions = transactions.slice();
		const transactionIndexes = [transactions.findIndex((entry) => entry.id === transaction.id), transactions.findIndex((entry) => entry.id === transaction.destinyId)];
		newTransactions.splice(transactionIndexes[0], 1);
		newTransactions.splice(transactionIndexes[1], 1);
		dispatch({
			type: 'changeTransaction',
			newTransactions
		});
	}

	const handleSave = (transaction, edit) => {
		let newTransactions = transactions.slice();
		let mirrorType = transaction.type;
		let mirrorAmmount = transaction.ammount;
		switch (transaction.type) {
			case 'Cargo':
				mirrorType = 'Abono'
				break;
			case 'Abono':
				mirrorType = 'Cargo'
				break;
			case 'Pago':
				mirrorAmmount = transaction.ammount * (-1);
				break;
			default:
				break;
		}
		const newTransaction = {
			...transaction,
			status: 'Pendiente', account: 'Arrendatario'
		}
		const mirrorTransaction = {
			...transaction,
			status: 'Pendiente', account: transaction.destiny, destiny: 'Arrendatario', type: mirrorType, ammount: mirrorAmmount
		}
		if (edit) {
			const transactionIndexes = [transactions.findIndex((entry) => entry.id === transaction.id), transactions.findIndex((entry) => entry.id === transaction.destinyId)];
			newTransactions.splice(transactionIndexes[0], 1, newTransaction);
			newTransactions.splice(transactionIndexes[1], 1, mirrorTransaction);
		} else {
			newTransactions.push({ ...newTransaction, id: currentID, destinyId: currentID+1});
			newTransactions.push({ ...mirrorTransaction, id: currentID+1, destinyId: currentID});
			dispatch({
				type: 'changeID'
			});
		}
		dispatch({
			type: 'changeTransaction',
			newTransactions
		});
	}

	const handleChangePropertyFilter = (e) => {
		const newProperty = e.target.value;
		setProperty(newProperty);
	}

	const columns = [
		{title: "Estatus", field: "status", render: rowData => <Paper className={classes['status']}>{rowData.status}</Paper> },
		{title: "Propiedad", field: "property"},
		{title: "Tipo", field: "type", render: rowData => `${rowData.type} ${rowData.destiny}`},
		{title: "Categoría", field: "category"},
		{title: "Monto", field: "ammount", render: rowData => `${rowData.currency} ${rowData.ammount}`},
		{title: "Período Contable", field: "contableDate", render: rowData => format(rowData.contableDate, 'MMM yyyy') },
	]

	const actions = [
		{icon: Edit, tooltip: "Editar Transaccion", onClick: editTransaction },
		{icon: Delete, tooltip: "Eliminar Transaccion", onClick: deleteTransaction },
	]

	console.log('Transactions', transactions);

    return (
		<React.Fragment>
		<Container>
			<Grid container alignItems='flex-end'>
				<Grid item xs={4}>
					<Typography variant="h4">Arrendatario</Typography>
				</Grid>
				<Grid item xs={4}>
					<TextField select label='Filtro de Propiedad' fullWidth margin='dense' value={property} onChange={handleChangePropertyFilter}>
						<MenuItem key={0} value='Todas'>Todas</MenuItem>
						{Properties.map(option => (
							<MenuItem key={option.id} value={option.name}>{option.name}</MenuItem>
						))}
					</TextField>
				</Grid>
				<Grid item xs={4}>
					<Button onClick={viewConsolidated} className={classes.pullRight}>Consolidado</Button>
				</Grid>
			</Grid>
			<Divider light className={classes.divider}/>
			<Grid item xs={12}>
				<MaterialTable
					title="Transacciones"
					icons={MaterialIcons}
					columns={columns}
					data={tableData}
					actions={actions}
					options={{actionsColumnIndex: -1}}
					localization={{
						header: {actions: "Acciones"}
					}}
				/>
			</Grid>
		</Container>
		<Fab size="large" color="secondary" aria-label="add" className={classes.fab} onClick={createTransaction}>
          <Add />
        </Fab>
		<TransactionModal open={openTransaction} edit={edit} transaction={transaction} onClose={handleCloseTransaction} save={handleSave} account='Arrendatario' />
		<ConsolidatedModal open={openConsolidated} title="Arrendatario" onClose={handleCloseConsolidated} account='Arrendatario' />
		</React.Fragment>
    )
}
