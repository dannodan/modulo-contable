import React, { useState, useEffect } from 'react'
import { Dialog, DialogTitle, DialogActions, DialogContent, Button, Grid, Table, TableRow, TableHead, TableCell, TableBody, makeStyles } from '@material-ui/core'
import DateFnsUtils from "@date-io/date-fns";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { getState } from '../../services/contextService';
import { format } from 'date-fns/esm';

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing(3),
      overflowX: 'auto',
    },
    table: {
      minWidth: 650,
    },
    positive: {
        color: 'green'
    },
    negative: {
        color: 'red'
    }
}));

export default function ConsolidatedModal(props) {

    const [selectedDate, setSelectedDate] = useState(new Date());
    const [dateString, setDateString] = useState('');
    const [{transactions}, dispatch] = getState();
    const [consolidatedData, setConsolidatedData] = useState([]);

    useEffect(() => {
        const accountTransactions = transactions.filter(entry => entry.account === props.account)
            .filter(entry => format(entry.contableDate, 'MM yyyy') === format(selectedDate, 'MM yyyy'))
        setConsolidatedData(accountTransactions);
    }, [props.account, transactions, selectedDate])

    const classes = useStyles();

    const handleDateChange = (e, string) => {
        setSelectedDate(e);
        setDateString(dateString);
        console.log(e, string);
    }

    const generateConsolidated = () => {
        const positive = consolidatedData.map(entry => (entry.type === 'Abono' || (entry.type === 'Pago' && entry.ammount > 0)) ? entry.ammount : 0)
            .reduce((acc, cur) => (acc + cur), 0);
        const negative = consolidatedData.map(entry => (entry.type === 'Cargo' || (entry.type === 'Pago' && entry.ammount <= 0)) ? Math.abs(entry.ammount) : 0)
            .reduce((acc, cur) => (acc + cur), 0);
        return {positive, negative, total: (Number(positive) - Number(negative))}
    }

    const renderCells = () => {
        const consolidatedObject = generateConsolidated();
        return (
            <TableRow key={1}>
                <TableCell align="center">${consolidatedObject.positive}</TableCell>
                <TableCell align="center">${consolidatedObject.negative}</TableCell>
                <TableCell align="center" className={(consolidatedObject.total > 0) ? classes.positive : classes.negative}>${consolidatedObject.total}</TableCell>
            </TableRow>
        );
    }

    console.log(consolidatedData);

    return (
        <Dialog open={props.open} onClose={props.onClose} aria-labelledby='transaction-dialog-title' fullWidth maxWidth='md'>
            <DialogTitle>Consolidado Mensual {props.title}</DialogTitle>
            <DialogContent>
                <Grid container fluid>
                    <Grid item xs={12}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                variant="inline"
                                openTo="year"
                                views={["year", "month"]}
                                label="Mes y Año"
                                helperText="Fecha limite del Consolidado"
                                value={selectedDate}
                                onChange={handleDateChange}
                                autoOk
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item className={classes.root}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">Entrada</TableCell>
                                    <TableCell align="center">Salida</TableCell>
                                    <TableCell align="center">Total</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {renderCells()}
                            </TableBody>
                        </Table>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose}>Close</Button>
            </DialogActions>
        </Dialog>
    )
}
