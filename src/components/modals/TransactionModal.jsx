import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField, Grid, Checkbox, FormControlLabel, MenuItem } from '@material-ui/core';
import DateFnsUtils from "@date-io/date-fns";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Properties, Currency, Type, Categories, Accounts } from '../../constants/constants';

export default function TransactionModal(props) {

    const [transaction, setTransaction] = useState({

    });

    useEffect(() => {
        if (!!props.transaction) {
            setTransaction(props.transaction);
        }
    }, [props.transaction]);

    const handleChange = (field) => (e) => {
        switch (field) {
            case 'contableDate':
            case 'transactionDate':
            case 'overDate':
                setTransaction({...transaction, [field]: e});
                break;
            case 'imputable':
                setTransaction({...transaction, [field]: e.target.checked});
                break;
            default:
                setTransaction({...transaction, [field]: e.target.value});
                break;
        }
    }

    const saveTransaction = () => {
        console.log('Salvando:', transaction);
        props.save(transaction, props.edit);
        props.onClose();
    }

    return (
        <Dialog open={props.open} onClose={props.onClose} aria-labelledby='transaction-dialog-title' fullWidth maxWidth='md'>
            <DialogTitle>{props.edit ? 'Editar' : 'Agregar'} Transaccion</DialogTitle>
            <DialogContent dividers>
                <Grid container fluid spacing={2}>
                    <Grid item xs={4}>
                        <TextField select label='Propiedad' fullWidth margin='normal' value={transaction.property} onChange={handleChange('property')} disabled={props.edit}>
                            {Properties.map(option => (
                                <MenuItem key={option.id} value={option.name}>{option.name}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField select label='Destino' fullWidth margin='normal' value={transaction.destiny} onChange={handleChange('destiny')} >
                            {Accounts
                                .filter(option => (option.name !== props.account))
                                .map(option => (
                                <MenuItem key={option.id} value={option.name}>{option.name}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField select label='Tipo' fullWidth margin='normal' value={transaction.type} onChange={handleChange('type')} >
                            {Type.map(option => (
                                <MenuItem key={option.id} value={option.name}>{option.name}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField select label='Categoria' fullWidth margin='normal' value={transaction.category} onChange={handleChange('category')} >
                            {Categories.map(option => (
                                <MenuItem key={option.id} value={option.name}>{option.name}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={2}>
                        <TextField select label='Moneda' fullWidth margin='normal' value={transaction.currency} onChange={handleChange('currency')}>    
                            {Currency.map(option => (
                                <MenuItem key={option.id} value={option.symbol}>{option.name}</MenuItem>
                            ))}
                        </TextField> 
                    </Grid>
                    <Grid item xs={2}>
                        <TextField id='ammount' label='Monto' value={transaction.ammount} onChange={handleChange('ammount')} fullWidth margin='normal'/>
                    </Grid>
                    <Grid item xs={2}>
                        <FormControlLabel
                            labelPlacement="top"
                            control={
                                <Checkbox checked={transaction.imputable} onChange={handleChange('imputable')} />
                            }
                            label="Imputable"
                            
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker label='Fecha de Transaccion' value={transaction.transactionDate} onChange={handleChange('transactionDate')} format='dd/MM/yyyy' margin='normal' fullWidth/>
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker label='Fecha de Vencimiento' value={transaction.overDate} onChange={handleChange('overDate')} format='dd/MM/yyyy' margin='normal' fullWidth/>
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                openTo="year"
                                views={["year", "month"]}
                                label="Periodo Contable"
                                value={transaction.contableDate}
                                onChange={handleChange('contableDate')}
                                margin='normal'
                                autoOk
                                fullWidth
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button color='secondary' onClick={props.onClose}>Cancelar</Button>
                <Button color='primary' onClick={saveTransaction}>{(props.edit ? 'Guardar' : 'Crear')}</Button>
            </DialogActions>
        </Dialog>
    )
}
