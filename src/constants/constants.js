export const Properties = [
    {id: 1, name: 'Propiedad 1'},
    {id: 2, name: 'Propiedad 2'},
    {id: 3, name: 'Propiedad 3'},
    {id: 4, name: 'Propiedad 4'},
    {id: 5, name: 'Propiedad 5'},
];

export const Accounts = [
    {id: 1, name: 'Propiedad'},
    {id: 2, name: 'Arrendatario'},
    {id: 3, name: 'Operador'},
]

export const Categories = [
    {id: 1, name: 'Arriendo Mensual'},
    {id: 2, name: 'Ajuste Valor de Arriendo'},
    {id: 3, name: 'Descuento por reembolso de pago'},
    {id: 4, name: 'Pago total del Periodo'},
    {id: 5, name: 'Cargo por Cuenta'},
    {id: 6, name: 'Reparacion'},
    {id: 7, name: 'Contribucion'},
    {id: 8, name: 'Dividendo'},
    {id: 9, name: 'Servicio Basico'},
    {id: 10, name: 'Otro'},
];

export const Type = [
    {id: 1, name: 'Abono'},
    {id: 2, name: 'Cargo'},
    {id: 3, name: 'Pago'},
];

export const Currency = [
    {id: 1, name: 'Peso', code: 'CLP', symbol: '$'},
    {id: 2, name: 'Dolar', code: 'USD', symbol: '$'},
];