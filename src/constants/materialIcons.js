import {
    Add,
    Check,
    Clear,
    DeleteOutline,
    ChevronRight,
    Edit,
    SaveAlt,
    FilterList,
    FirstPage,
    LastPage,
    ChevronLeft,
    Search,
    ArrowUpward,
    Remove,
    ViewColumn
} from '@material-ui/icons';

export const MaterialIcons = {
    Add,
    Check,
    Clear,
    Delete: DeleteOutline,
    DetailPanel: ChevronRight,
    Edit,
    Export: SaveAlt,
    Filter: FilterList,
    FirstPage,
    LastPage,
    NextPage: ChevronRight,
    PreviousPage: ChevronLeft,
    ResetSearch: Clear,
    Search,
    SortArrow: ArrowUpward,
    ThirdStateCheck: Remove,
    ViewColumn
}